#pragma once
#include "DataStructures.h"


class Component
{
public:
	Component();
	~Component();
	/*what does this do??*/
	//virtual ComponentType GetType() const = 0;
	//virtual Component GetComponent();
	ComponentType mType;
	virtual void Initialize();
	virtual void Destroy();

};

