//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.h
// definition of MyGame, an implementation of exGameInterface
//

#include "Game/Public/GameInterface.h"
#include "Engine/Public/EngineTypes.h"
#include <vector>
#include "Shape.h"
#include "GameObject.h"
#include "PuzzleHandler.h"
#include <time.h>
#include "TrafficLightSM.h"
#include "StopWatchStart.h"
#include "StopWatchPause.h"
#include "StopWatchReset.h"
#include "StopWatchSM.h"
#include "DataStructures.h"
#include "VendingSelectState.h"
#include "VendingInputState.h"
#include "VendingOutput.h"
#include "VendingSM.h"
//-----------------------------------------------------------------
//-----------------------------------------------------------------

class MyGame : public exGameInterface
{
public:

								MyGame();
	virtual						~MyGame();

	virtual void				Initialize( exEngineInterface* pEngine );

	virtual const char*			GetWindowName() const;
	virtual void				GetClearColor( exColor& color ) const;

	virtual void				OnEvent( SDL_Event* pEvent );
	virtual void				OnEventsConsumed();

	virtual void				Run( float fDeltaT );

private:

	exEngineInterface*			mEngine;

	int							mFontID;
	int*						Player1Score;
	int*						Player2Score;


	bool						mUpP1;
	bool						mDownP1;
	bool						mLeftP1;
	bool						mRightP1;
	bool						canChange;
	bool						sStart;
	bool						sPause;
	bool						sReset;
	bool						inputUp;
	bool						inputDown;
	bool						vInput;
	bool						vSelect;
	bool						vOutput;
	bool						bCanInteract;


	exVector2					mTextPosition;
	std::vector <Shape*>		mObjectsToRender;
	std::vector <GameObject*>   mGameObjects;

	float						PlayerSpeed;
	float*						StopWatchTime;
	float						PlayerCoin;

	time_t						start;



	exColor						TextColor1;
	exColor						TextColor2;

	PuzzleHandler*				myPuzzleHandler;
	PopType						selectedPop;


	//stateMachines//
	TrafficLightSM*				TSM;
	StopWatchSM*				SSM;
	VendingSM*					VSM;


	StopWatchStart*				SSM_START;
	StopWatchPause*				SSM_PAUSE;
	StopWatchReset*				SSM_RESET;

	VendingInputState*			VSM_INPUT;
	VendingSelectState*			VSM_SELECT;
	VendingOutput*				VSM_OUTPUT;



};
