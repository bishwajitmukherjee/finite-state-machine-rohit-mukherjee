#pragma once
#include <vector>
#include "Component.h"
#include <string>
class GameObject
{
public:
	GameObject();
	~GameObject();
	std::vector <Component*> mComponents;//std vector to store all the components
	void Initialize();//initializes the Game Object.
	void AddComponent(Component* pComponent);//this functions adds more components to the game object.
	Component* GetComponentOfType(ComponentType cmpType);//this function returns the component of type or null if not found.
	std::string tag;
	GameObject* GetGameObjectWithTag(std::string chckTag);
	
};

