#include "GameObject.h"


GameObject::GameObject()
{
}


GameObject::~GameObject()// runs a loop which runs through all the components attached and destroys and clears the memory
{
	for (Component* pComponent : mComponents)
	{
		pComponent->Destroy();

		delete pComponent;
	}
}

void GameObject::AddComponent(Component * pComponent)// adds components to the vector mComponents in the game Object.
{
	mComponents.push_back(pComponent);
}

Component * GameObject::GetComponentOfType(ComponentType cmpType)
{
	for (auto &cmp : mComponents) // access by reference to avoid copying
	{
		if (cmp->mType == cmpType)
		{
			return cmp;
		}
	}
	return nullptr;
}

GameObject * GameObject::GetGameObjectWithTag(std::string chckTag)
{
	if (tag == chckTag)return this;
	return nullptr;
}

void GameObject::Initialize()// function to initialize the game object.
{
	for (Component* pComponent : mComponents)
	{
		pComponent->Initialize();
	}
}


