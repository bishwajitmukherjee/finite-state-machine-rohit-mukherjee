//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.cpp
// implementation of MyGame, an implementation of exGameInterface
//PG 17 Bishwajit Mukherjee

#include "Game/Private/Game.h"

#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include "GameManager.h"
#include "InputManager.h"
#include "GameObject.h"
#include "Box.h"
#include "Ball.h"
#include "Physics.h"
#include "Transform.h"
#include "Trigger.h"
#include "MathLib.h"
#include "Renderer.h"
#include "TrafficLightState.h"
#include "StopWatch.h"
#include <memory>



//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* gWindowName = "Sample EngineX Game";


//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::MyGame()
	: mEngine( nullptr )
	, mFontID( -1 )
	, mUpP1( false )
	, mDownP1( false )
	,inputUp(false)
	,inputDown(false)
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::~MyGame()
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::Initialize( exEngineInterface* pEngine )
{
	canChange = true;
	mEngine = pEngine;
	StopWatchTime = new float();
	*StopWatchTime = 0.0f;
	mFontID = mEngine->LoadFont( "crackman front.ttf", 60 );
	selectedPop = PopType::Coke;
	Player1Score = new int();
	Player2Score = new int();
	*Player1Score = 0;
	*Player2Score = 0;
	mTextPosition.x = 50.0f;
	mTextPosition.y = 50.0f;
	PlayerCoin = 0;
	bCanInteract = false;

	PlayerSpeed = 200.0f;
	GameObject *Player = new GameObject();
	GameObject *PaddleTwo = new GameObject();
	GameObject *WallOne = new GameObject();
	GameObject *WallTwo = new GameObject();
	GameObject* WallThree = new GameObject();
	GameObject* WallFour = new GameObject();
	GameObject* Block = new GameObject();
	GameObject *Light = new GameObject();
	GameObject* StopWatch = new GameObject();
	GameObject* LightBack = new GameObject();
	GameObject* StopWatchBack = new GameObject();
	GameObject* VendingMachineBack = new GameObject();
	GameObject* VendingMachineOutput = new GameObject();
	Shape *tempShape = new Shape();
	Physics *tempPhysicsComponent = new Physics();
	//initializing Player
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine,0,exColor{ {255,192,203,255} }, 1, exVector2{ 20.0f,30.0f });
	Player->AddComponent(tempShape);
	Player->AddComponent(new Transform(exVector2{ 100.0f,500.0f }));
	tempPhysicsComponent->mMaxVelocity = PlayerSpeed;
	tempPhysicsComponent->mLinearDrag =10.0f;
	tempPhysicsComponent->mEnableGravity = true;
	Player->AddComponent(tempPhysicsComponent);
	Player->AddComponent(new Trigger(exVector2{ 25.0f,35.0f }));
	Player->tag = "PlayerOne";
	mGameObjects.push_back(Player);

	//initializing Wall1
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {255,165,0,250} }, 1, exVector2{ 700.0f,40.0f });
	WallOne->AddComponent(tempShape);
	WallOne->AddComponent(new Transform(exVector2{ 400.0f,30.0f }));
	WallOne->AddComponent(new Physics());
	WallOne->tag = "WallOne";
	mGameObjects.push_back(WallOne);

	//initializing Wall2
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {255,165,0,250} }, 1, exVector2{300.0f,40.0f });
	WallTwo->AddComponent(tempShape);
	WallTwo->AddComponent(new Transform(exVector2{100.0f,570.0f }));
	WallTwo->AddComponent(new Physics());
	WallTwo->tag = "WallTwo";
	mGameObjects.push_back(WallTwo);

	//initializing Wall3
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {255,165,0,250} }, 1, exVector2{ 300.0f,40.0f });
	WallThree->AddComponent(tempShape);
	WallThree->AddComponent(new Transform(exVector2{ 700.0f,570.0f }));
	WallThree->AddComponent(new Physics());
	WallThree->tag = "WallThree";
	mGameObjects.push_back(WallThree);


	//initializing Puzzle
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {250,0,100,250} }, 1, exVector2{ 200.0f,40.0f });
	Block->AddComponent(tempShape);
	Block->AddComponent(new Transform(exVector2{400.0f,570.0f }));
	Block->AddComponent(new Physics());
	Block->tag = "Puzzle";
	mGameObjects.push_back(Block);
	
	//initializing the light game object
	tempShape = tempShape->CreateShape(ShapeType::circle, mEngine, 20.0f, exColor{ {0,250,0,250} });
	Light->AddComponent(tempShape);
	Light->AddComponent(new Transform(exVector2{ 75.0f,100.0f }));
	Light->tag = "Light";
	mGameObjects.push_back(Light);

	//initializing the StopWatch game object
	tempShape = tempShape->CreateShape(ShapeType::circle, mEngine, 60.0f, exColor{ {0,0,0,250} });
	StopWatch->AddComponent(tempShape);
	StopWatch->AddComponent(new Transform(exVector2{ 400.0f,85.0f }));
	StopWatch->tag = "StopWatch";
	mGameObjects.push_back(StopWatch);
	//initializing the StopWatchBAcK game object
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {0,0,0,250} }, 0, exVector2{ 100.0f,50.0f });
	StopWatchBack->AddComponent(tempShape);
	StopWatchBack->AddComponent(new Transform(exVector2{ 400.0f,85.0f }));
	StopWatchBack->tag = "StopWatchBack";
	mGameObjects.push_back(StopWatchBack);

	//initializing traffic light background
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {0,0,0,250} }, 1, exVector2{ 50.0f,100.0f });
	LightBack->AddComponent(tempShape);
	LightBack->AddComponent(new Transform(exVector2{ 75.0f,100.0f }));
	LightBack->tag = "LightBack";
	mGameObjects.push_back(LightBack);

	//initializing the VendingMachine game object
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {0,0,0,250} }, 3, exVector2{ 100.0f,200.0f });
	VendingMachineBack->AddComponent(tempShape);
	VendingMachineBack->AddComponent(new Transform(exVector2{ 700.0f,450.0f }));
	VendingMachineBack->AddComponent(new Trigger(exVector2{ 150.0f,200.0f }));
	VendingMachineBack->tag = "VendingMachineBack";
	mGameObjects.push_back(VendingMachineBack);

	//initializing the VendingMachine Output game object
	tempShape = tempShape->CreateShape(ShapeType::box, mEngine, 0, exColor{ {0,0,0,250} }, 3, exVector2{ 50.0f,100.0f });
	VendingMachineOutput->AddComponent(tempShape);
	VendingMachineOutput->AddComponent(new Transform(exVector2{ 700.0f,500.0f }));
	VendingMachineOutput->tag = "VendingMachineOutput";
	mGameObjects.push_back(VendingMachineOutput);

	//initializing the TrafficLightStateMachine
	TSM = new TrafficLightSM();
	TrafficLightState* TSM_RED = new TrafficLightState(TSM, Light, exColor{ {250,0,0,250} },3.0f,0);
	TrafficLightState* TSM_YELLOW = new TrafficLightState(TSM, Light, exColor{ {250,250,0,250} }, 3.0f,1);
	TrafficLightState* TSM_GREEN = new TrafficLightState(TSM, Light, exColor{ {0,250,0,250} }, 5.0f,2);
	TSM_RED->mNextState = TSM_YELLOW;
	TSM_YELLOW->mNextState = TSM_GREEN;
	TSM_GREEN->mNextState = TSM_RED;
	TSM->Initialize(TSM_RED);
	myPuzzleHandler = new PuzzleHandler(Block, exVector2{ 400,570 }, exVector2{400,200});
	start = time(0);


	SSM = new StopWatchSM();
	SSM_START = new StopWatchStart(SSM, StopWatchBack, StopWatchTime,0);
	SSM_PAUSE = new StopWatchPause(SSM, StopWatchBack, StopWatchTime,1);
	SSM_RESET = new StopWatchReset(SSM, StopWatchBack, StopWatchTime,2);

	SSM->Initialize(SSM_START);


	VSM = new VendingSM();
	VSM_INPUT = new VendingInputState(VSM, VendingMachineBack,&PlayerCoin,&inputUp ,&inputDown,&bCanInteract);
	VSM_SELECT = new VendingSelectState(VSM, VendingMachineBack, &PlayerCoin, &inputUp, &inputDown, &bCanInteract, &selectedPop);
	VSM_OUTPUT = new VendingOutput(VSM,VendingMachineOutput, &PlayerCoin, &bCanInteract,&selectedPop);
	VSM->Initialize(VSM_SELECT);


}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* MyGame::GetWindowName() const
{
	return "State Machine";
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::GetClearColor( exColor& color ) const
{
	color.mColor[0] = 1;
	color.mColor[1] = 68;
	color.mColor[2] = 33;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEvent( SDL_Event* pEvent )
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEventsConsumed()
{
	int nKeys = 0;
	const Uint8 *pState = SDL_GetKeyboardState( &nKeys );


	//PLAYER 1 CONTROLS
	mUpP1 = pState[SDL_SCANCODE_W];
	mDownP1 = pState[SDL_SCANCODE_S];
	mLeftP1 = pState[SDL_SCANCODE_A];
	mRightP1 = pState[SDL_SCANCODE_D];
	sStart = pState[SDL_SCANCODE_1];
	sPause = pState[SDL_SCANCODE_2];
	sReset = pState[SDL_SCANCODE_3];
	inputUp = pState[SDL_SCANCODE_UP];
	inputDown = pState[SDL_SCANCODE_DOWN];
	vSelect = pState[SDL_SCANCODE_4];
	vInput = pState[SDL_SCANCODE_5];
	vOutput = pState[SDL_SCANCODE_E];
}

//-----------------------------------------------------------------
/*
	W-Jump
	A-Left
	D-Right
	1-To start Stopwatch
	2-To pause Stopwatch
	3-To reset Stopwatch
	UpArrow-To navigate through selection/ increase coin
	DownArrow-To navigate through selection/ decrease coin
	4-to go to drink selection state
	5-to go to coin enter state
	E-to interact with the vending machine

	============================================
	traffic light has a cooldown of 3 seconds on red and yellow and 5 seconds on green.
	stop watch changes color based on state
	vending machine outputs the color of can based on available coins
	cost of Drinks
	COKE:10,PEPSI:20,SPRITE:30
					
*/



//-----------------------------------------------------------------

void MyGame::Run( float fDeltaT )
{

	TSM->Update(fDeltaT);
	SSM->Update(fDeltaT);
	VSM->Update(fDeltaT);

	//stopwatch Input
	if (sStart && !(sPause||sReset))
	{
		SSM->Next(SSM_START);
	}
	if (sPause && !(sStart || sReset))
	{
		SSM->Next(SSM_PAUSE);
	}
	if (sReset && !(sPause || sStart))
	{
		SSM->Next(SSM_RESET);
	}
	GameObject* VM = new GameObject();
	for (auto& currentObj : mGameObjects) // access by reference to avoid copying
	{
		if (currentObj->GetGameObjectWithTag("VendingMachineBack") != nullptr)
		{
			VM = currentObj->GetGameObjectWithTag("VendingMachineBack");
			break;
		}
	}
	bCanInteract = dynamic_cast<Trigger*>(VM->GetComponentOfType(ComponentType::Trigger))->IsOverlapping("PlayerOne");
	//VendingMachine Input
	if (bCanInteract)
	{
		if (vInput && !(vOutput || vSelect))
		{
			canChange = false;
			VSM->Next(VSM_INPUT);
		}
		if (vSelect && !(vOutput || vInput))
		{
			VSM->Next(VSM_SELECT);
			canChange = false;
		}
		if (vOutput && !(vSelect || vInput) && !canChange)
		{
			VSM->Next(VSM_OUTPUT);
			canChange = true;
		}
		else if(!vOutput)
		{
			canChange = false;
		}
	}

	//--------------------------------------INPUT MANAGER-------------------------------------------//

	InputManager::InputManagerUpdate(mUpP1, mDownP1, mLeftP1, mRightP1,mGameObjects);
	
	//-----------------------------------Colission Checking--------------------------------------//
		Physics::ColissionTests(mGameObjects, fDeltaT);

	//--------------------------------------------------PHYSICS UPDATE----------------------------------------------------//
		Physics::PhysicsTick(mGameObjects,fDeltaT);
		Trigger::TriggerTest(mGameObjects,fDeltaT);

	//---------------------------------------RENDER SYSTEM-------------------------------------------//
		Renderer::RenderUpdate(mGameObjects);
	//=======================================PuzzleSystem=====================================//
		if (dynamic_cast<TrafficLightState*>(TSM->GetCurrentState())->getIndex() == 2)
		{
			myPuzzleHandler->UP();
		}
		else
		{
			myPuzzleHandler->DOWN();
		}
		
		std::string str = std::to_string((int)*StopWatchTime);

		mEngine->DrawText(mFontID, exVector2{ 375,50 }, str.c_str(),exColor{ {250,0,250,250} }, 1);
		std::string coinAmount = std::to_string((int)PlayerCoin);

		mEngine->DrawText(mFontID, exVector2{ 675,350 }, coinAmount.c_str(), exColor{ {250,250,250,250} }, 1);

		std::string selectedItem;
		if ((int)selectedPop == 10)
		{
			selectedItem = "Coke";
		}
		if ((int)selectedPop == 20)
		{
			selectedItem = "Pepsi";
		}
		if ((int)selectedPop == 30)
		{
			selectedItem = "Sprite";
		}
		mEngine->DrawText(mFontID, exVector2{ 600,250 }, selectedItem.c_str(), exColor{ {250,250,250,250} }, 1);
}

