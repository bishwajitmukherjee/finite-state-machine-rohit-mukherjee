#pragma once

enum class ShapeType // enum class that stores diffrent shape types
{
	box, 
	circle
};


enum class ComponentType : int// enum class that stores component type to be used while creating a factory
{
	Transform = 0,
	Physics,
	Shape,
	Trigger
};

struct Vector2 // a vector2 structure to help store the transform in the Transform component.
{
	float x;
	float y;
};

enum class PopType : int //enum class to store drings and their price;
{
	Coke=10,
	Pepsi=20,
	Sprite=30
};