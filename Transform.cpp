#include "Transform.h"



Transform::Transform()
{
	mPosition.x = 0;
	mPosition.y = 0;
	mType = ComponentType::Transform;
}

Transform::Transform(exVector2 midPoint)
{
	mType = ComponentType::Transform;
	mPosition = midPoint;
}


Transform::~Transform()
{
}

void Transform::Initialize()
{

}

exVector2 & Transform::GetPosition()//function to return the current transform
{
	 return mPosition; 
}

void Transform::SetPosition(exVector2 newPos)//function to set the new position
{
	mPosition = newPos;
}
