#include "Box.h"



Box::Box(float point1, float point2, exColor color, int nthLayer, exEngineInterface* pEngine)
{
	width = point1;
	height = point2;
	c = color;
	nLayer = nthLayer;
	mEngine = pEngine;
	mType = ComponentType::Shape;
	mShapeType = ShapeType::box;
	resetColor = c;
}

Box::Box()
{
}


Box::~Box()
{
}

void Box::render(exVector2 midPoint)
{

	mEngine->DrawBox(exVector2{midPoint.x-(width/2),midPoint.y-(height/2)}, exVector2{ midPoint.x + (width / 2),midPoint.y + (height / 2) }, c, nLayer);
}
