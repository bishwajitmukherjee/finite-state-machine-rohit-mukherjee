#pragma once
#include "Game/Private/GameObject.h"
#include "Game/Public/GameInterface.h"
class PuzzleHandler
{
public:
 PuzzleHandler(GameObject* pPuzzle,exVector2 pUpPos,exVector2 pDownPos);
 ~PuzzleHandler();
 GameObject* mPuzzle;
 exVector2 upPos;
 exVector2 downPos;
 bool currentState;
 void UP();
 void DOWN();

};

