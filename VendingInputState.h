#pragma once
#include "VendingState.h"
class VendingInputState: public VendingState
{
public:
	VendingInputState(FSM* pFSM, GameObject* pDisplay,float* CoinCount,bool* up,bool* down,bool* pInteract);
	virtual void Update(float pDeltaT) override;
private:
	float* mCoinCount;
	bool* up;
	bool* down;
};

