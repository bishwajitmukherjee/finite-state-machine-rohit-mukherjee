#include "Physics.h"
//#include "Game/Private/DataStructures.h"
#include "Game/Private/GameObject.h"
#include "Game/Private/Component.h"
#include "Box.h"
#include "Ball.h"
#include "MathLib.h"


Physics::Physics()
{
	mType = ComponentType::Physics;
	mCanBounce = false;
	mVelocity = 0.0f;
	mDirection = exVector2{ 0,-1};
	mLinearDrag = 1;
	mMaxVelocity = 0;
	mEnableGravity = false;
	mHorizontalVelocity = 0.0f;
}

Physics::Physics(bool bounce)
{
	mType = ComponentType::Physics;
	mCanBounce = bounce;
	mVelocity = 0.0f;
	mDirection = exVector2{ 0,0 };
	mLinearDrag = 1;
	mMaxVelocity = 0;
}


Physics::~Physics()
{
}

exVector2 Physics::PhysicsUpdate(exVector2 centre,  float DeltaTime, float radius, exVector2 boxDimensions)
{
	centre.y += (mVelocity * mDirection.y * DeltaTime);
	centre.x += (mVelocity * mDirection.x * DeltaTime *0);
	
	if (!mCanBounce)
	{
		if (mEnableGravity)
		{
			if (mDirection.y <= 1)
			{
				mVelocity = clamp(mVelocity -= mLinearDrag, -90.0f, mMaxVelocity);
				//mHorizontalVelocity = clamp(mHorizontalVelocity -= mLinearDrag, 0.0f, mMaxVelocity);
			}
			else
			{
				mVelocity = clamp(mVelocity -= mLinearDrag, 0.0f, mMaxVelocity);
				//mHorizontalVelocity = clamp(mHorizontalVelocity -= mLinearDrag, 0.0f, mMaxVelocity);
			}
		}
		else
		{
			mVelocity = clamp(mVelocity -= mLinearDrag, 0.0f, mMaxVelocity);
		}
		
	}
	

	return centre;
}

void Physics::PhysicsTick(std::vector<GameObject*> mGameObjects,float DeltaTime)
{
	for (auto &currentObj : mGameObjects) // access by reference to avoid copying
	{

		Transform *tempTrans = dynamic_cast<Transform*>(currentObj->GetComponentOfType(ComponentType::Transform));
		if (currentObj->GetComponentOfType(ComponentType::Physics) != nullptr)
		{
			Physics* tempPhysics = dynamic_cast<Physics*>(currentObj->GetComponentOfType(ComponentType::Physics));
			tempTrans->SetPosition(tempPhysics->PhysicsUpdate((tempTrans->GetPosition()), DeltaTime));
			tempTrans->SetPosition({ tempTrans->GetPosition().x += tempPhysics->mVelocity * tempPhysics->mDirection.x*DeltaTime,tempTrans->GetPosition().y += tempPhysics->mVelocity * tempPhysics->mDirection.y*DeltaTime });

		}
	}

}



//colission Checking
void Physics::ColissionTests(std::vector<GameObject*> mGameObjects,float fDeltaT)
{
	for (auto &currentObj : mGameObjects) // access by reference to avoid copying
	{
		if (currentObj->GetComponentOfType(ComponentType::Shape) != nullptr)
		{
			Shape *Sone = dynamic_cast<Shape*>(currentObj->GetComponentOfType(ComponentType::Shape));//stores the first shape
			if (currentObj->GetComponentOfType(ComponentType::Physics) != nullptr)
			{
				Transform *midOne = dynamic_cast<Transform*>(currentObj->GetComponentOfType(ComponentType::Transform));
				for (auto &currentObj2 : mGameObjects) // access by reference to avoid copying
				{
					if (currentObj2->GetComponentOfType(ComponentType::Shape) != nullptr)
					{
						Shape *Stwo = dynamic_cast<Shape*>(currentObj2->GetComponentOfType(ComponentType::Shape));//stores the second shape
						if (currentObj2->GetComponentOfType(ComponentType::Physics) != nullptr)
						{
							Transform *midTwo = dynamic_cast<Transform*>(currentObj2->GetComponentOfType(ComponentType::Transform));
							//If Statement to check shape of first box
							if (Sone->mShapeType == ShapeType::box)
							{
								if (Stwo->mShapeType == ShapeType::box)//check box box colission
								{
									Box* box1 = dynamic_cast<Box*>(Sone);
									Box* box2 = dynamic_cast<Box*>(Stwo);
									if (box1 != box2)
									{
										//check if the boxes are within bounds of each other.
										if ((abs(midOne->GetPosition().x - midTwo->GetPosition().x) * 2 < (box1->width + box2->width)) &&
											(abs(midOne->GetPosition().y - midTwo->GetPosition().y) * 2 < (box1->height + box2->height)))
										{
											Physics* tempPhysics = dynamic_cast<Physics*>(currentObj->GetComponentOfType(ComponentType::Physics));
											if (tempPhysics->mDirection.y != 0.0f)
											{
												tempPhysics->mDirection.x = -tempPhysics->mDirection.x;
												tempPhysics->mDirection.y = 1.0f;
												tempPhysics->mVelocity = -tempPhysics->mVelocity;
												midOne->SetPosition(tempPhysics->PhysicsUpdate((midOne->GetPosition()), fDeltaT));
											}
										}
									}

								}
								
							}

						}
					}
				}

			}
		}
	}
}







