#pragma once
#include "State.h"
#include "Game/Public/GameInterface.h"
#include "Shape.h"
class StopWatch: public State
{
public:

	StopWatch(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex);
	int getIndex();
	State* mNextState;
protected:
	GameObject* mWatchDisplay;
	float* mTime;
	float StoredTime;
	int index;
};

