#include "VendingState.h"

//base vending machine state
VendingState::VendingState(FSM* pFSM, GameObject* pDisplay)
	:State(pFSM)
	, mDisplay(pDisplay)
	,mNextState(nullptr)
	,canInteract(nullptr)
{
};
