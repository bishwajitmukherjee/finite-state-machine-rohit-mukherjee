#pragma once
#include "StopWatch.h"
class StopWatchPause :public StopWatch
{
public:
	StopWatchPause(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex);
	virtual void OnEnter() override;
};

