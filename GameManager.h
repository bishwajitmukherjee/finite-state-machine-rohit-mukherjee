#pragma once
#include "Engine/Public/EngineTypes.h"
#include "Engine/Public/EngineInterface.h"
#include "Game/Private/GameObject.h"
class GameManager
{
public:
	GameManager();
	~GameManager();
	static void UpdateScore(int* Score1, int* Score2, exEngineInterface* pEngine,int mFontID,GameObject* ball, exColor col1, exColor col2);

};

