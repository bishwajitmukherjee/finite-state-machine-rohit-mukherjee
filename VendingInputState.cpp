#include "VendingInputState.h"
//this state manages the coin input of the vending machine
VendingInputState::VendingInputState(FSM* pFSM, GameObject* pDisplay, float* pCoinCount,bool* pUp, bool* pDown,bool* pInteract)
	:VendingState(pFSM,pDisplay)
	,mCoinCount(pCoinCount)
	,up(pUp)
	,down(pDown)
{
	canInteract = pInteract;
}

void VendingInputState::Update(float pDeltaT)
{
	if (!*canInteract)return;
	if (*up && !*down)
	{
		*mCoinCount = *mCoinCount + (pDeltaT*10);
	}
	else if (*down && !*up)
	{
		*mCoinCount = *mCoinCount - (pDeltaT * 10);
	}
	*mCoinCount = clamp(*mCoinCount,0.0f,100.0f);
}
