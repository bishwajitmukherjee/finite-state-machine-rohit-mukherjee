#pragma once
#include "VendingState.h"
#include "Game/Private/DataStructures.h"
class VendingSelectState : public VendingState
{
public:
	VendingSelectState(FSM* pFSM, GameObject* pDisplay, float* CoinCount, bool* up, bool* down,bool* pInteact,PopType* pPop);
	virtual void OnEnter() override;
	virtual void Update(float pDeltaT) override;
private:
	float* mCoinCount;
	bool* up;
	bool* down;
	float index;
	PopType* mPopType;
};
