#include "VendingSelectState.h"
//this state manages the selection of brand of pop

VendingSelectState::VendingSelectState(FSM* pFSM, GameObject* pDisplay, float* pCoinCount, bool* pUp, bool* pDown,bool* pInteract,PopType* pPop)
	:VendingState(pFSM,pDisplay)
	,mCoinCount(pCoinCount)
	,up(pUp)
	,down(pDown)
	,index(0.0f)
	,mPopType(pPop)
{
	canInteract = pInteract;
}

void VendingSelectState::OnEnter()
{
	index = 0;
}

void VendingSelectState::Update(float pDeltaT)
{
	if (!*canInteract)return;
	if (*up && !*down)
	{
		index = index + (pDeltaT*5);
	}
	else if (*down && !*up)
	{
		index = index - (pDeltaT * 5);
	}
	index = clamp(index, 0.0f, 100.0f);
	int curretnIndex = ((((int)index) % 3) * 10)+10;
	*mPopType = (PopType)curretnIndex;
}
