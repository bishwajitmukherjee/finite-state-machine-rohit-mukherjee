#include "TrafficLightState.h"
#include "Game/Public/GameInterface.h"
//this sate manages all the traffic light behavior
TrafficLightState::TrafficLightState(FSM* pFSM, GameObject* pLight, exColor pColor,float pTime,int pIndex)
	: State(pFSM)
	, mLight(pLight)
	, mColor(pColor)
	,mTime(pTime)
	,StoredTime(pTime)
	,index(pIndex)
{
};

void TrafficLightState::OnEnter()
{
	dynamic_cast<Shape*>(mLight->GetComponentOfType(ComponentType::Shape))->c = mColor;
}

void TrafficLightState::Update(float pDelta)
{
	mTime -= pDelta;
	if (mTime <= 0)
	{
		//State* pNewState = mFSM->Create<GreenState>();
		mFSM->Next(mNextState);
	}
}

void TrafficLightState::OnExit()
{
	mTime = StoredTime;
}

int TrafficLightState::getIndex()
{
	return index;
}




