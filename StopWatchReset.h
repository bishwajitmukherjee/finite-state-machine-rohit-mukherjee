#pragma once
#include "StopWatch.h"
class StopWatchReset:public StopWatch
{
public:
	StopWatchReset(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex);
	virtual void OnEnter() override;
};

