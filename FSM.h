#pragma once
#include "Game/Private/GameObject.h"
class State;
class FSM
{
public:

	FSM();
	~FSM();

	void Initialize(State* pState);
	void Next(State* pState);

	virtual void Update(float fDeltaT);

	//
	// the syntax used below is called variadic templates, it's a C++11 feature
	// read about it here: http://www.cplusplus.com/articles/EhvU7k9E/
	//

	template<class T, typename... params>
	T* Create(params... xs)
	{
		return new T(this, xs...);
	}
	State* GetCurrentState();
protected:

	State* mCurrent;
	State* mNext;

};

