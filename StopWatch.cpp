#include "StopWatch.h"
//base stop Watch State

StopWatch::StopWatch(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex)
	: State(pFSM)
	, mWatchDisplay(pWatchDisplay)
	, mTime(pTime)
	, StoredTime(*pTime)
	, index(pIndex)
{
};

int StopWatch::getIndex()//returns the light index to lower and up the puzzle
{
	return index;
}
