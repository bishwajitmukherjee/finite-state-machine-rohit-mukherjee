#pragma once
#include "Game/Private/Component.h"
#include "Engine/Public/EngineTypes.h"
#include "Engine/Public/EngineInterface.h"
#include <vector>
#include "Game/Private/GameObject.h"
#include"Transform.h"
class Trigger:public Component
{
public:
	Trigger(exVector2 dimensions);
	~Trigger();
	std::vector<GameObject*> mInBoundObjects;
	exVector2 mTrigerDimensions;
	bool IsOverlapping(std::string pGO);

	
	static void TriggerTest(std::vector<GameObject*> mGameObjects, float fDeltaT);

};

