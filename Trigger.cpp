#include "Trigger.h"
#include <algorithm>

Trigger::Trigger(exVector2 dimensions)
	:mTrigerDimensions(dimensions)
{
	mType = ComponentType::Trigger;
}

Trigger::~Trigger()
{
}

//function to find if the trigger is overlapping the desired object.
bool Trigger::IsOverlapping(std::string pGO)
{
	for (auto& currentObj : mInBoundObjects) // access by reference to avoid copying
	{
		if (currentObj->GetGameObjectWithTag(pGO))
		{
			return true;
		}
	}
	return false;
}

void Trigger::TriggerTest(std::vector<GameObject*> mGameObjects, float fDeltaT)
{



	for (auto& currentObj : mGameObjects) // access by reference to avoid copying
	{
		if (currentObj->GetComponentOfType(ComponentType::Trigger) != nullptr)
		{
			Trigger* Tone = dynamic_cast<Trigger*>(currentObj->GetComponentOfType(ComponentType::Trigger));//stores the first Trigger

				Transform* midOne = dynamic_cast<Transform*>(currentObj->GetComponentOfType(ComponentType::Transform));
				for (auto& currentObj2 : mGameObjects) // access by reference to avoid copying
				{
					bool canAdd = true;
					int removeAtIndex = 0;
					
					if (currentObj2->GetComponentOfType(ComponentType::Trigger) != nullptr)
					{
						Trigger* Ttwo = dynamic_cast<Trigger*>(currentObj2->GetComponentOfType(ComponentType::Trigger));//stores the second Trigger
						if (currentObj2->GetComponentOfType(ComponentType::Trigger) != nullptr)
						{
							Transform* midTwo = dynamic_cast<Transform*>(currentObj2->GetComponentOfType(ComponentType::Transform));

									if (Tone != Ttwo)
									{//begin overlap
										//check if the boxes are within bounds of each other.
										if ((abs(midOne->GetPosition().x - midTwo->GetPosition().x) * 2 < (Tone->mTrigerDimensions.x + Ttwo->mTrigerDimensions.y)) &&
											(abs(midOne->GetPosition().y - midTwo->GetPosition().y) * 2 < (Tone->mTrigerDimensions.y + Ttwo->mTrigerDimensions.y)))
										{
											for (auto& trig : Tone->mInBoundObjects) // access by reference to avoid copying
											{
												if (trig == currentObj2)
												{
													canAdd = false;
													break;
												}
											}
											if(canAdd)Tone->mInBoundObjects.push_back(currentObj2);
										}
										else
										{//end overlap
											canAdd = true;
											for (auto& trig : Tone->mInBoundObjects) // access by reference to avoid copying
											{
												if (trig == currentObj2)
												{
													canAdd = false;
													//removeAtIndex++;
													break;
												}
												removeAtIndex++;
											}
											if (!canAdd)
											{
												Tone->mInBoundObjects.erase(Tone->mInBoundObjects.begin() + removeAtIndex);
											}
										}
									}
						}
					}

			}
		}
	}
}
