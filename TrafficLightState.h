#pragma once
#include "State.h"
#include "Game/Public/GameInterface.h"
#include "Shape.h"
class TrafficLightState : public State
{
	public:

		TrafficLightState(FSM* pFSM, GameObject* pLight, exColor pColor,float pTime,int pIndex);
		virtual void OnEnter() override;
		virtual void Update(float pDelta) override;
		virtual void OnExit() override;
		int getIndex();
		State* mNextState;
	protected:
		GameObject* mLight;
		exColor mColor;
		float mTime;
		float StoredTime;
		int index;
		
};

