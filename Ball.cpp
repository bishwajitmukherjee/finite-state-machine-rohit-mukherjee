#include "Ball.h"



Ball::Ball(float radius, exColor color, int nthLayer, exEngineInterface * pEngine)
{
	mRadius = radius;
	c = color;
	nLayer = nthLayer;
	mEngine = pEngine;
	mType = ComponentType::Shape;
	mShapeType = ShapeType::circle;
}

Ball::Ball()
{
}


Ball::~Ball()
{
}

void Ball::render(exVector2 midPoint)
{
	mEngine->DrawCircle(midPoint, mRadius, c, nLayer);
}
