#pragma once
#include "Game/Private/GameObject.h"
#include "vector"
class InputManager
{
public:
	InputManager();
	~InputManager();
	static void InputManagerUpdate(bool mUp1, bool mDown1, bool mUp2, bool mDown2,std::vector<GameObject*> mGameObjects);
};

