#pragma once
#include "Shape.h"
class Ball:public Shape
{
public:
public:
	Ball(float radius, exColor color, int nthLayer, exEngineInterface* pEngine);
	Ball();
	~Ball();
	virtual void render(exVector2 midPoint) override;
	float mRadius;
};

