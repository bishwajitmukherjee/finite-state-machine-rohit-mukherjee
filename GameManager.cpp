#include "GameManager.h"

#include "Engine/Public/EngineTypes.h"
#include "Engine/Public/EngineInterface.h"
#include "Transform.h"
#include "PuzzleHandler.h"
#include <string>

GameManager::GameManager()
{

}


GameManager::~GameManager()
{
}

void GameManager::UpdateScore(int* Score1,int* Score2,exEngineInterface* mEngine,int mFontID,GameObject* ball,exColor col1,exColor col2)
{
	mEngine->DrawText(mFontID, exVector2{ 225,250 }, "Just Pong", exColor{ 240,94,35,255 }, 3);

	
	Transform* ballTransform = static_cast<Transform*>(ball->GetComponentOfType(ComponentType::Transform));
	if (ballTransform->GetPosition().x < 0.0f)
	{
		*Score2 = *Score2+1;
		ballTransform->SetPosition({300,250});
	}
	else if (ballTransform->GetPosition().x >800.0f)
	{
		*Score1 = *Score1 + 1;
		ballTransform->SetPosition({ 300,250 });
	}


	std::string str1 = std::to_string(*Score1);
	const char *c1 = str1.c_str();

	std::string str2 = std::to_string(*Score2);
	const char *c2 = str2.c_str();

	mEngine->DrawText(mFontID, exVector2{ 150,250 }, c1,col1, 1);
	mEngine->DrawText(mFontID, exVector2{ 600,250 }, c2,col2, 1);
}
