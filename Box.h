#pragma once
#include "Shape.h"
class Box:public Shape
{
	public:
	Box(float height, float width, exColor color, int nthLayer, exEngineInterface* pEngine);
	Box();
	~Box();
	virtual void render(exVector2 midPoint) override;
	float height,width;
	exColor resetColor;
};

