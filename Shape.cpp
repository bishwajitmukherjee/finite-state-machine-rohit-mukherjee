#include "Shape.h"
#include "Box.h"
#include "Ball.h"


Shape::Shape()
{
}


Shape::~Shape()
{
}

Shape* Shape::CreateShape(ShapeType pType, exEngineInterface* pEngine, float CircleVar, exColor color, int nthLayer, exVector2 BoxVar)
{
	switch (pType)
	{
	case ShapeType::box:	
		return (new Box(BoxVar.x, BoxVar.y, color, nthLayer, pEngine));
		break;
	case ShapeType::circle:
		return (new Ball(CircleVar, color,nthLayer,pEngine));
		break;
	default:
	return new Shape();
		break;
	}

}

void Shape::Initialize()
{
}

void Shape::render(exVector2 a)
{
}

