#include "Renderer.h"
#include "Shape.h"
#include "Transform.h"


Renderer::Renderer()
{
}


Renderer::~Renderer()
{
}

void Renderer::RenderUpdate(std::vector<GameObject*> mGameObjects)
{
	for (auto &currentObj : mGameObjects) // access by reference to avoid copying
	{
		if (currentObj->GetComponentOfType(ComponentType::Shape) != nullptr)
		{
			Shape *objectToRender = dynamic_cast<Shape*>(currentObj->GetComponentOfType(ComponentType::Shape));
			if (currentObj->GetComponentOfType(ComponentType::Transform) != nullptr)
			{
				Transform *midP = dynamic_cast<Transform*>(currentObj->GetComponentOfType(ComponentType::Transform));
				objectToRender->render(midP->GetPosition());
			}
		}
	}
}
