#include "FSM.h"
#include "State.h"


FSM::FSM()
	: mCurrent(nullptr)
	, mNext(nullptr)
{
}

FSM::~FSM()
{
}

void FSM::Initialize(State* pState)
{
	// assert when mCurrent is not null?
	mCurrent = pState;
	mCurrent->OnEnter();
}

void FSM::Next(State* pState)
{
	if (mNext != nullptr)
	{
		mNext = nullptr;
	}

	mNext = pState;
}

void FSM::Update(float fDeltaT)
{
	if (mNext != nullptr)
	{
		mCurrent->OnExit();
		mCurrent = mNext;
		mCurrent->OnEnter();
		mNext = nullptr;
	}

	mCurrent->Update(fDeltaT);
}

State* FSM::GetCurrentState()
{
	return mCurrent;
}
