#pragma once
#include "Game/Private/Component.h"
#include "Engine/Public/EngineTypes.h"
class Transform :public Component
{
public:
	Transform();
	Transform(exVector2 midPoint);
	~Transform();
	virtual void Initialize() override;
	exVector2& GetPosition();
	void SetPosition(exVector2 newPos);

private:

	exVector2 mPosition;

};

