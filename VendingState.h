#pragma once
#include "State.h"
#include "Game/Public/GameInterface.h"
#include "MathLib.h"
class VendingState:public State
{
public:
	VendingState(FSM* pFSM, GameObject* pDisplay);
	State* mNextState;
	bool* canInteract;
protected:
	GameObject* mDisplay;
};

