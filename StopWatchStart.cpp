#include "StopWatchStart.h"
//this sate manages Starting Stop watch 
StopWatchStart::StopWatchStart(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex)
	:StopWatch(pFSM,pWatchDisplay,pTime,pIndex)
{
};
void StopWatchStart::OnEnter()
{
	dynamic_cast<Shape*>(mWatchDisplay->GetComponentOfType(ComponentType::Shape))->c = exColor{{0,250,0,250}};
}

void StopWatchStart::Update(float pDelta)
{
	*mTime += pDelta;
}

void StopWatchStart::OnExit()
{
}