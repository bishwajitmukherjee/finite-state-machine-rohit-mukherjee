#pragma once
#include "VendingState.h"
#include "Game/Private/DataStructures.h"
class VendingOutput:public VendingState
{
public:
	VendingOutput(FSM* pFSM, GameObject* pDisplay, float* CoinCount, bool* pInteract, PopType* pPop);
	virtual void OnEnter() override;
	virtual void OnExit() override;
private:
	float* mCoins;
	PopType* mPopType;
};

