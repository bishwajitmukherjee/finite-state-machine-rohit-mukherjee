#pragma once
#include "Engine/Public/EngineTypes.h"
#include "Game/Private/Component.h"
#include "Game/Private/GameObject.h"
#include "Shape.h"
#include"Transform.h"
#include <vector>
class Physics:public Component
{
public:
	Physics();
	Physics(bool bounce);
	~Physics();
	float mVelocity;
	exVector2 mDirection;
	bool mCanBounce;
	float mMaxVelocity;
	float mHorizontalVelocity;
	float mLinearDrag;
	bool mEnableGravity;

	exVector2 PhysicsUpdate(exVector2 centre,float DeltaTime,float radius = 0.0f, exVector2 boxDimensions = exVector2{0,0});
	static void PhysicsTick(std::vector<GameObject*> mGameObject,float DeltaTime);
	static void ColissionTests(std::vector<GameObject*> mGameObjects,float fDeltaT);


};

