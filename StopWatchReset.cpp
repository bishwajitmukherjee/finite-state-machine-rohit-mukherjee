#include "StopWatchReset.h"
//this sate manages Reseting the stop 
StopWatchReset::StopWatchReset(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex)
:StopWatch(pFSM, pWatchDisplay, pTime, pIndex)
{
};
void StopWatchReset::OnEnter()
{
	*mTime = 0.0f;
	dynamic_cast<Shape*>(mWatchDisplay->GetComponentOfType(ComponentType::Shape))->c = exColor{ {250,0,0,250} };
}
