#include "VendingOutput.h"
#include "Shape.h"

//this state manages the droping of pop cans and decreasing the coin

VendingOutput::VendingOutput(FSM* pFSM, GameObject* pDisplay, float* CoinCount,bool* pInteract, PopType* pPop)
	:VendingState(pFSM,pDisplay)
	,mCoins(CoinCount)
	,mPopType(pPop)
{
	canInteract = pInteract;
}

void VendingOutput::OnEnter()
{
	if (!*canInteract)return;
	if ((int)*mPopType <= *mCoins)
	{
		*mCoins = *mCoins - (int)*mPopType;
		if ((int)*mPopType == 10)
		{
			dynamic_cast<Shape*>(mDisplay->GetComponentOfType(ComponentType::Shape))->c = exColor{ {250,0,0,250} };
		}
		if ((int)*mPopType == 20)
		{
			dynamic_cast<Shape*>(mDisplay->GetComponentOfType(ComponentType::Shape))->c = exColor{ {0,0,250,250} };
		}
		if ((int)*mPopType == 30)
		{
			dynamic_cast<Shape*>(mDisplay->GetComponentOfType(ComponentType::Shape))->c = exColor{ {0,250,0,250} };
		}
	}
}

void VendingOutput::OnExit()
{
	dynamic_cast<Shape*>(mDisplay->GetComponentOfType(ComponentType::Shape))->c = exColor{ {0,0,0,250} };
}
