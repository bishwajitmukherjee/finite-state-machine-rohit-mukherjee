#pragma once
#include "FSM.h"
class State
{
public:

	State(FSM* pFSM);
	virtual ~State() { }

	virtual void OnEnter() { }			// gets called when enters
	virtual void OnExit() { }			// gets called when exits

	virtual void Update(float fDeltaT) { }

protected:

	FSM* mFSM;
	//FSMComponent* m_component;

};
