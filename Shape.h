#pragma once
#include "Game/Private/Component.h"
#include "Engine/Public/EngineTypes.h"
#include "Engine/Public/EngineInterface.h"
class Shape:public Component
{
public:
	Shape();
	~Shape();
	Shape* CreateShape(ShapeType pType, exEngineInterface* pEngine, float point2 = 0.0f, exColor color = exColor{ {1,1,1,1} }, int nthLayer = 0, exVector2 point1 = exVector2{ 0,0 });
	virtual void Initialize() override;
	virtual void render(exVector2 a);
	ShapeType mShapeType;
	exVector2 p1;
	exVector2 p2;
	exColor c;
	int nLayer;
	exEngineInterface* mEngine;
	float timer;
};

