#pragma once
#include "Game/Private/GameObject.h"
#include <vector>
class Renderer
{
public:
	Renderer();
	~Renderer();
	static void RenderUpdate(std::vector<GameObject*> mGameObjects);
};

