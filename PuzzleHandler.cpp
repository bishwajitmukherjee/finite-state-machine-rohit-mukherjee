#include "PuzzleHandler.h"
#include "Transform.h"
//manages the upping and lowering of the puzzle .. was not done with state machine but could be refactored to be used with state machines.
PuzzleHandler::PuzzleHandler(GameObject* pPuzzle, exVector2 pUpPos, exVector2 pDownPos)
{
	upPos = pUpPos;
	downPos = pDownPos;
	mPuzzle = pPuzzle;
	currentState = true;
}

PuzzleHandler::~PuzzleHandler()
{
}


void PuzzleHandler::UP()
{
	dynamic_cast<Transform*>(mPuzzle->GetComponentOfType(ComponentType::Transform))->SetPosition(upPos);
}

void PuzzleHandler::DOWN()
{
	dynamic_cast<Transform*>(mPuzzle->GetComponentOfType(ComponentType::Transform))->SetPosition(downPos);
}

