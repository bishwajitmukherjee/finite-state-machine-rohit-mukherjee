#include "InputManager.h"
#include "Engine/Public/EngineTypes.h"
#include "Transform.h"
#include "Physics.h"


InputManager::InputManager()
{
}


InputManager::~InputManager()
{
}

void InputManager::InputManagerUpdate(bool mUp,bool mDown,bool mLeft,bool mRight,std::vector<GameObject*> mGameObjects)
{
	GameObject* player = nullptr;

	for (auto& currentObj : mGameObjects) // access by reference to avoid copying
	{
		if (currentObj->GetGameObjectWithTag("PlayerOne") != nullptr)
		{
			player = currentObj;
		}
	}
	Transform* tempTrans = dynamic_cast<Transform*>(player->GetComponentOfType(ComponentType::Transform));
	Physics* tempPhysics = dynamic_cast<Physics*>(player->GetComponentOfType(ComponentType::Physics));
	//PLAYER 1 INPUT 
	if (mUp)
	{
		if (player->GetComponentOfType(ComponentType::Physics) != nullptr)
		{
			if (tempPhysics->mDirection.y >= 0.0f)
			{
				tempPhysics->mVelocity = tempPhysics->mMaxVelocity;
				tempPhysics->mDirection = exVector2{ 0,-1 };
			}
		}

	}
	if (mLeft)
	{
		if (player->GetComponentOfType(ComponentType::Physics) != nullptr)
		{
			//if (tempPhysics->mDirection.y >=0)
			{
				//tempPhysics->mHorizontalVelocity = tempPhysics->mMaxVelocity*100;
				//tempPhysics->mDirection = exVector2{ -1,tempPhysics->mDirection.y };
				tempTrans->SetPosition(exVector2{ (tempTrans->GetPosition().x) -= 2.0f, tempTrans->GetPosition().y });

			}
		}

	}
	if (mRight)
	{
		if (player->GetComponentOfType(ComponentType::Physics) != nullptr)
		{
			//if (tempPhysics->mDirection.y >= 0)
			{
				//tempPhysics->mHorizontalVelocity = tempPhysics->mMaxVelocity*100;
				//tempPhysics->mDirection = exVector2{ 1,tempPhysics->mDirection.y };
				tempTrans->SetPosition(exVector2{ (tempTrans->GetPosition().x) += 2.0f, tempTrans->GetPosition().y });
			}
		}

	}
	if((mLeft || mRight || mUp))
	{
		tempPhysics->mHorizontalVelocity = 0.0f;
		tempPhysics->mDirection = exVector2{ 0,-1 };
		//tempPhysics->mVelocity =-tempPhysics->mVelocity;
	}

	
	
}
