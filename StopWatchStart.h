#pragma once
#include "StopWatch.h"
class StopWatchStart:public StopWatch
{
public:
	StopWatchStart(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex);
	virtual void OnEnter() override;
	virtual void Update(float pDelta) override;
	virtual void OnExit() override;
};

