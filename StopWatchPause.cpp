#include "StopWatchPause.h"
//this sate manages pausing of the stop watch
StopWatchPause::StopWatchPause(FSM* pFSM, GameObject* pWatchDisplay, float* pTime, int pIndex)
	:StopWatch(pFSM, pWatchDisplay, pTime, pIndex)
{
};
void StopWatchPause::OnEnter()
{
	dynamic_cast<Shape*>(mWatchDisplay->GetComponentOfType(ComponentType::Shape))->c = exColor{ {250,250,0,250} };
}

